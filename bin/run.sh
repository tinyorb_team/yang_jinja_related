# This code developed by tinyorb organisation
# Check the license for further information
# *********************************************

#!/bin/bash

set -e
path="$(readlink -f ${BASH_SOURCE[0]})"
path="$(dirname $path)"
main_path="$(dirname $path)"

to_pt=$(sudo docker ps | grep topt | xargs echo)

if [[ "$to_pt" == "" ]]; then
  echo "no container, installing..."
  sudo docker rm topt || true
  sudo docker run --name=topt -d tinyorb/yangloaded
fi

case $1 in
test)
  sudo docker exec -t topt python3 /tmp/srcs/main.py test
  ;;
reload)
  sudo docker exec -t topt rm -rf /tmp/srcs || true
  sudo docker cp $main_path/srcs topt:/tmp/
  ;;
generate)
  sudo docker exec -t topt python3 /tmp/srcs/main.py $@
  ;;
bypass)
  readarray -d " " -t arg < <(echo "$@")
  arg=${arg[@]:1}
  sudo docker exec -t topt $arg
  ;;
add)
  case $2 in
  template)
    name=$3
    tpath=$4
    if [ -f $tpath ]; then
      sudo docker cp $tpath topt:/tmp/srcs/resource/template
      sudo docker exec -t topt python3 /tmp/srcs/main.py $@
    else
      echo "file not found"
    fi
    ;;
  datastore)
    name=$3
    dpath=$4
    if [ -f $dpath ]; then
      sudo docker cp $dpath topt:/tmp/srcs/resource/datastore
      sudo docker exec -t topt python3 /tmp/srcs/main.py $@
    else
      echo "file not found"
    fi
    ;;
  *)
    echo "Incorrect command, try add template|data <name> <path>"
    ;;
  esac
  ;;
backup)
  sudo docker cp topt:/tmp/srcs/resource/datastore .
  sudo docker cp topt:/tmp/srcs/resource/template .
  sudo docker cp topt:/tmp/srcs/spec.json .
  ;;
list)
  case $2 in
  template)
    sudo docker exec -t topt python3 /tmp/srcs/main.py $@
    ;;
  datastore)
    sudo docker exec -t topt python3 /tmp/srcs/main.py $@
    ;;
  *)
    echo "Incorrect command, try list template|data"
  esac
  ;;
*)
  echo "Incorrect command, try test|reload|upload|generate|bypass"
  ;;
esac
