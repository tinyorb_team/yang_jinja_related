# This code developed by tinyorb organisation
# Check the license for further information
# *********************************************

#!/bin/bash
set -e
set -u
path="$(readlink -f ${BASH_SOURCE[0]})"
path="$(dirname $path)"
path=$path"/container_resource"
if [ -d $path ]; then
  cd $path
  sudo docker build -t tinyorb/yangloaded .
else
  err "error"
  exit 1
fi
