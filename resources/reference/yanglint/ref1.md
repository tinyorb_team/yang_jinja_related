focal (1) yanglint.1.html#examples

Provided by: yang-tools_0.16.105-3build1_amd64 bug
[link](https://manpages.ubuntu.com/manpages/focal/man1/yanglint.1.html#formats)
NAME

       yanglint - YANG lint tool

SYNOPSIS

       yanglint
       yanglint [OPTIONS] [-f { yang | yin | tree } ] FILE ...
       yanglint [OPTIONS] [-f { xml | json } ] SCHEMA...  FILE...

DESCRIPTION

       yanglint  is  a  command-line tool for validating and converting YANG schemas and the YANG
       modeled data. For a simple use, it validates the provided file and if  the  output  format
       specified,  it  converts  input  data into the output format. If started with no argument,
       yanglint opens interactive environment where the user is allowed to work with schemas  and
       data in a more complex way.

OPTIONS

       -h, --help
              Outputs usage help and exits.

       -v, --version
              Outputs the version number and exits.

       -V, --verbose
              Increases the verbosity level. If not specified, only errors are printed, with each
              appearance it adds: warnings, verbose messages, debug messages  (if  compiled  with
              debug information).

       -p PATH, --path=PATH
              Specifies  search  path  for  getting  imported modules or included submodules. The
              option can be used multiple times. The current working directory and  path  of  the
              module being added is used implicitly.

       -s, --strict
              Changes handling of unknown data nodes - instead of silently ignoring unknown data,
              error is printed and data parsing fails. This option applies only on data parsing.

       -f FORMAT, --format=FORMAT
              Converts the content of the input FILEs into the specified FORMAT. If no OUTFILE is
              specified, the data are printed on the standard output. Only the compatible formats
              for the input FILEs are allowed, see the section FORMATS.

       -o OUTFILE, --output=OUTFILE
              Writes the output data into the specified OUTFILE. The option can be used  only  in
              combination with --format option. In case of converting schema, only a single input
              schema FILE is allowed. In case of data input FILEs, input is  merged  and  printed
              into a single OUTFILE.

       -F FEATURES, --features=FEATURES
              Specifies  the list of enabled features in the format MODULE:[FEATURE,...]. In case
              of processing multiple modules, the option can be used repeatedly. To  disable  all
              the features, use an empty list specified for the particular module.

       -d MODE, --default=MODE
              Print  data  with  default  values, according to the MODE (to print attributes, the
              ietf-netconf-with-defaults model must be loaded). The MODE is one of the following:
               · all             - add missing default nodes
               · all-tagged      - add missing default nodes and mark all the default nodes  with
              the attribute
               · trim            - remove all nodes with a default value
               · implicit-tagged - add missing nodes and mark them with the attribute

       -t TYPE, --type=TYPE
              Specify data tree type in the input data FILEs. The TYPE is one of the following:
               ·  auto             -  Resolve  data type (one of the following) automatically (as
              pyang does). Applicable only on XML input data.
               · data            - Complete datastore with status data (default type).
               · config          - Configuration datastore (without status data).
               · get             - Result of the NETCONF <get> operation.
               · getconfig       - Result of the NETCONF <get-config> operation.
               · edit            - Content of the NETCONF <edit-config> operation.
               · rpc             - Content of the NETCONF <rpc> message, defined  as  YANG's  rpc
              input statement.
               ·  rpcreply         -  Reply  to the RPC. This is just a virtual TYPE, for parsing
              replies, 'auto' must be used since the data FILEs are expected in pairs.
                                   The first input data FILE  is  expected  as  'rpc'  TYPE,  the
              second FILE is expected as reply to the previous RPC.
               ·  notif            - Notification instance (content of the <notification> element
              without <eventTime>.

       -r FILE, --running=FILE]
              Optional  parameter  for  'rpc'  and  'notif'  TYPEs,  the  FILE  contains  running
              configuration  datastore  data  referenced from the RPC/Notification. The same data
              apply to all input data FILEs. Note that the file  is  validated  as  'data'  TYPE.
              Special value '!' can be used as FILE argument to ignore the external references.

       -y YANGLIB_PATH
              Specify  path  to  a  yang-library  data  file (XML or JSON) describing the initial
              context.  If provided, yanglint loads the modules according to the content  of  the
              yang-library data tree.  Otherwise, an empty content with only the internal libyang
              modules is used. This does not  limit  user  to  load  another  modules  explicitly
              specified as command line parameters.

FORMATS

       There are two types of formats to use.

       Schemas
              In  case of schemas, the content can be converted into the 'yang', 'yin' and 'tree'
              formats.  As  input,  only  YANG  and  YIN  files  are  accepted.  Note,  that  the
              corresponding file extension is required.

       Data
              In case of YANG modeled data, the content can be converted between 'xml' and 'json'
              formats. Remember that the corresponding  file  extension  of  the  input  file  is
              required.

EXAMPLES

       · Open interactive environment:
             yanglint

       · Convert YANG model into YIN and print it to the stdout:
             yanglint --format=yin ./ietf-system.yang

       · Convert ietf-system configuration data from XML to JSON:
             yanglint    --format=json    --type=config   --output=data.json   ./ietf-system.yang
         ./data.xml

SEE ALSO

       https://github.com/CESNET/libyang (libyang homepage and Git repository)

AUTHORS

       Radek Krejci <rkrejci@cesnet.cz>, Michal Vasko <mvasko@cesnet.cz>

COPYRIGHT

       Copyright © 2015-2017 CESNET, a.l.e.

