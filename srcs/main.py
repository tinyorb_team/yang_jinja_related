# This code developed by tinyorb organisation
# Check the license for further information
# *********************************************

import os
import sys
import json
from lib.jinja_engine import parse

if __name__ == "__main__":
    prnt = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(prnt, "spec.json")) as f:
        specs = json.load(f)
    if sys.argv[1] == "test":
        sys.path.append(os.path.join(prnt, "test"))
        cases = [x.replace(".py", "") for x in os.listdir(os.path.join(prnt, "test"))
                 if x[:4] == "test"]
        for case in cases:
            exec("import {}".format(case))
        for case in cases:
            mdl = sys.modules[case]
            tc = mdl.__dict__["case"]
            with open(os.path.join(prnt, specs["templates"][tc["template"]]["path"])) as f:
                template = f.read()
            with open(os.path.join(prnt, tc["data_path"])) as f:
                data = json.load(f)
            with open(os.path.join(prnt, tc["expectation"])) as f:
                expect = f.read()
            result = parse(template, data)
            _result = result.replace(" ", "").replace("\r", "").replace("\n", "")
            _expect = expect.replace(" ", "").replace("\r", "").replace("\n", "")
            assert _result == _expect, "expect {}, found {}".format(expect, result)
            print("Test case pass: {}".format(tc))

    elif sys.argv[1] == "generate":
        with open(os.path.join(prnt, specs["templates"][sys.argv[2]]["path"])) as f:
            template = f.read()
        with open(os.path.join(prnt, specs["datastore"][sys.argv[3]]["path"])) as f:
            data = json.load(f)
        result = parse(template, data)
        print("Rendered output: \n{}".format(result))

    elif sys.argv[1] == "add":
        if sys.argv[2] == "template":
            print("please make sure template has been on path {}".format(os.path.join("resource", "template")))
            name = sys.argv[3]
            path = os.path.join(prnt, os.path.join(os.path.join("resource", "template"), os.path.basename(sys.argv[4])))
            specs["templates"][name] = {"path": path}
        elif sys.argv[2] == "datastore":
            print("please make sure data has been on path {}".format(os.path.join("resource", "datastore")))
            name = sys.argv[3]
            path = os.path.join(prnt, os.path.join(os.path.join("resource", "datastore"), os.path.basename(sys.argv[4])))
            specs["datastore"][name] = {"path": path}
        with open(os.path.join(prnt, "spec.json"), "w") as f:
            json.dump(specs, f)

    elif sys.argv[1] == "list":
        if sys.argv[2] == "template":
            print(list(specs["templates"].keys()))
        elif sys.argv[2] == "datastore":
            print(list(specs["datastore"].keys()))
    else:
        print("unknown request {}".format(sys.argv[1:]))
