from jinja2 import Template


def parse(template, data):
    return Template(template).render(data)
